//
//  LoginViewModel.swift
//  TradeV
//
//  Created by Anuj on 17/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//
import Foundation
import UIKit
import Alamofire

class LoginViewModel: NSObject
{
 
    class func isValidAllFields(email : String , password : String, success : @escaping([String:String]) -> Void)
    {
        if email.characters.count == 0
        {
            TAAlertController.alert(APP_NAME, message: EMAIL_ALERT_MESSAGE)
        }
        else if password.characters.count == 0
        {
            TAAlertController.alert(APP_NAME, message: PASSWORD_ALERT_MESSAGE)
        }
        else
        {
            /*When all field is valid , then login API will call
              we will send the parameter in request key, client id, password in md5 form
              when API gives success reponse , then AUTHOK will be true
             When API gives failure response , then AuthOk will be false */
            
            let loginRequest = [KEY_DEFAULT : LOGIN_KEY , USER_ID : email , PASSWORD : password.md5()] as [String : Any]
            WebManager.requestPOSTURL(strURL: BASE_URL+LOGIN_KEY_URL, params: loginRequest, success: { (response) in
                if let response = response as? [String: String]
                {
                    success(response)
                }
            })
            { (error) in
                TAAlertController.alert(error.domain, message: error.localizedDescription)
            }
        }
    }
}
