//
//  LeftMenuViewModel.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class LeftMenuViewModel: NSObject {
     var sideMenuArray :[SideMenuModel] = []
    
    func fillDataInModel(success : @escaping([SideMenuModel]) -> Void)
    {
        let menuArray = [["segmentName": ""],["segmentName": "Stock" , "segmentImageName" : "" , "segmentArray" : []], ["segmentName": "Index","segmentImageName" : "" , "segmentArray" : []], ["segmentName": "Currency","segmentImageName" : "" , "segmentArray" : []], ["segmentName": "Metal","segmentImageName" : "" , "segmentArray" : []], ["segmentName": "Equity","segmentImageName" : "" , "segmentArray" : []], ["segmentName": "My Alert" , "segmentImageName" : "alert" , "segmentArray" : ["Stock" , "Index" , "Currency" , "Metal" , "Equity"]], ["segmentName": "Message Center" , "segmentImageName" : "messageCenter" , "segmentArray" : []],["segmentName": "Favourite" , "segmentImageName" : "favCopy" , "segmentArray" : ["Stock" , "Index" , "Currency" , "Metal" , "Equity"]], ["segmentName": "Performance Reports" , "segmentImageName" : "forma1_3" , "segmentArray" : []], ["segmentName": "Subscription" , "segmentImageName" : "subscriptionmenu" , "segmentArray" : []],["segmentName": "About Us" , "segmentImageName" : "forma1" , "segmentArray" : []],["segmentName": "Help" , "segmentImageName" : "forma1Copy" , "segmentArray" : []],["segmentName": "Terms and Conditions" , "segmentImageName" : "forma1_2" , "segmentArray" : []],["segmentName": "Contact Us" , "segmentImageName" : "shape1" , "segmentArray" : []] , ["segmentName": "Logout" , "segmentImageName" : "forma1Copy4" , "segmentArray" : []]]
        
        for menuDict in menuArray
        {
            self.sideMenuArray.append(SideMenuModel.fillDataInModel(dict: menuDict))
        }
        
        success(self.sideMenuArray)
        
    }

}
