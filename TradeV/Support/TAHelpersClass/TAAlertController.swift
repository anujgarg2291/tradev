//
//  TAAlertController.swift
//  TradeV
//
//  Created by Anuj on 17/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

@objc open class TAAlertController: NSObject {
    
    //==========================================================================================================
    // MARK: - Singleton
    //==========================================================================================================
    
    class var instance : TAAlertController {
        struct Static {
            static let inst : TAAlertController = TAAlertController ()
        }
        return Static.inst
    }
    
    
    //==========================================================================================================
    // MARK: - Class Functions
    //==========================================================================================================
    
    @discardableResult
    open class func alert(_ title: String) -> UIAlertController {
        return alert(title, message: "")
    }
    
    @discardableResult
    open class func alert(_ title: String, message: String) -> UIAlertController {
        return alert(title, message: message, acceptMessage: "OK", acceptBlock: {
            // Do nothing
        })
    }
    
    @discardableResult
    open class func alert(_ title: String, message: String, acceptMessage: String, acceptBlock: @escaping () -> ()) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let acceptButton = UIAlertAction(title: acceptMessage, style: .default, handler: { (action: UIAlertAction) in
            acceptBlock()
        })
        alert.addAction(acceptButton)
        
        TAUtility.topMostController()?.present(alert, animated: true, completion: nil)
        return alert
    }

}
