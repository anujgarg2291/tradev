//
//  TAUtility.swift
//  TradeV
//
//  Created by Anuj on 17/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class TAUtility: NSObject {

//==========================================================================================================
// MARK: - Handling of Scrolling with KeyBoard
//==========================================================================================================
    class func showKeyBoard(notification: NSNotification, scrollView:UIScrollView)
    {
        var userInfo =  notification.userInfo!
        let size: AnyObject? = userInfo[UIKeyboardFrameBeginUserInfoKey] as AnyObject?
        let keyboardSize = size!.cgRectValue.size
        var contentInsets:UIEdgeInsets;
        if (UIInterfaceOrientationIsPortrait(UIApplication.shared.statusBarOrientation))
        {
            contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
        }
        else
        {
            contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.width, 0.0);
        }
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        
    }
    
    class func hideKeyBoard(scrollView:UIScrollView)
    {
        scrollView.contentInset = UIEdgeInsets.zero;
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero;
    }
    
    
//==========================================================================================================
// MARK: - Get TopMost Controller
//==========================================================================================================
    public class func topMostController() -> UIViewController?
    {
        
        var presentedVC = UIApplication.shared.keyWindow?.rootViewController
        while let pVC = presentedVC?.presentedViewController
        {
            presentedVC = pVC
        }
        
        if presentedVC == nil
        {
            print("TAUtility Error: You don't have any views set. You may be calling in viewdidload. Try viewdidappear.")
        }
        return presentedVC
    }
    
    
//==========================================================================================================
// MARK: - Save Data in UserDefault
//==========================================================================================================
    
    class func saveDataInUserDefault(dict :[String:String])
    {
       UserDefaults.standard.set(dict, forKey: USER_INFO)
       UserDefaults.standard.synchronize()
    }
    
    
    class func getUserDataInUserDefault() -> [String : String]
    {
        if let userData = UserDefaults.standard.value(forKey: USER_INFO) as? [String:String]
        {
            return userData
        }
        
        return [:]
    }
}
