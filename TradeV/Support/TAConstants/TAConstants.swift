//
//  TAConstants.swift
//  TradeV
//
//  Created by Anuj on 16/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

enum FILE_NAME : String {
    case INDEX       = "IndexListVC"
    case CURRENCY    = "CurrencyListVC"
    case EQUITY      = "EquityListVC"
}

    let APP_NAME                                    =      "Trade-V"
    let kAppDelegate                                =       UIApplication.shared.delegate as! AppDelegate
//MARK: StoryBoard Identifier
    let HOME_VC_IDENTIFIER                           =     "HomeViewController"
    let LOGIN_VC_IDENTIFIER                          =     "LoginVC"
    let LEFT_MENU_VC_IDENTIFIER                      =     "LeftMenuViewVc"
    let ACCOUNT_INFO_VC_IDENTIFIER                   =     "AccountInfoVc"
    let FORGOT_PASSWORD_VC_IDENTIFIER                =     "ForgotPasswordVC"
    let INDEX_LIST_VC_IDENTIFIER                     =     "IndexListVC"
    let METAL_LIST_VC_IDENTIFIER                     =     "MetalListVC"
    let EQUITY_LIST_VC_IDENTIFIER                    =     "EquityListVC"
    let CURRENCY_LIST_VC_IDENTIFIER                  =     "CurrencyListVC"
    let STOCK_DETAIL_VC_IDENTIFIER                   =     "StockDetailVC"
    let CURRENCY_DETAIL_VC_IDENTIFIER                =     "CurrencyDetailController"
    let GUIDE_VC_IDENTIFIER                          =     "GuideVC"

//MARK: Screen UI Info Label

    let FORGOT_PASSWORD_INFO                         =    "Please enter your registered Email ID where \nyour password will be sent."
    let PRODUCT_AUTH_MESSAGE                         =    "Do you wish to activate this product?"
    let PRODUCT_ACTIVATED_ON_OTHER_DEVICE_MESSAGE    =    " The product is activated on another platform. Do you wish to activate it for website application? (Activation Remaining : )."

//MARK: Float Constants
    let LEFT_PADDING_VALUE       : CGFloat           =     16

//MARK: Color Constants

   let PLACEHOLDER_TEXT_COLOR                       =     UIColor(hex: 0xffffff, alpha: 0.5)


//MARK: Alert messages

   let EMAIL_ALERT_MESSAGE                           =     "Please enter an email."
   let PASSWORD_ALERT_MESSAGE                        =     "Please enter an password."
   let EMAIL_VALID_MESSAGE                           =     "Please enter an valid email."



//MARK: API Key

   let LOGIN_KEY                                     =     "Jj25q7IRkk"
   let SEGEMENT_KEY                                  =     "vimcSvW5aR"
   let KEY_DEFAULT                                   =     "key"
   let USER_ID                                       =     "userid"
   let PASSWORD                                      =     "password"
   let TRADE_V_SERVER_ERROR                          =     "TradeVServerError"
   let kUnknownError                                 =     "UnknownError"
   let AUTH_OK                                       =     "AuthOK"
   let INSTALLATION_ID                               =     "installationid"
   let MAIN_PRODUCT_ID                               =     "mainproductid"

//MARK: URL

    let BASE_URL                                      =     "http://www.s2analytics.com/ws/"
    let LOGIN_KEY_URL                                 =     "users/basic-authentication.php"
    let STOCK_LIST_URL                                =     "TradeV/trades-for-stock-futures.php?"
    let INDEX_LIST_URL                                =     "TradeV/trades-for-index.php"
    let METAL_LIST_URL                                =     "TradeV/trades-for-metals.php"
    let CURRENCY_LIST_URL                             =     "TradeV/trades-for-currency.php"
    let EQUITY_LIST_URL                               =     "TradeV/trades-for-equities.php"
    let PRODUCT_AUTHENTICATION_URL                    =     "products/product-authentication.php"
    let LAST_UPDATED_VALUE                            =     "TradeV/last-updated.php"

//MARK: User Default Key

    let USER_INFO                                    =     "USERINFO"


//MARK: LAST_UPADTED_SEGMENT_KEY

    let STOCK_KEY                   =     "StockFut"
    let CURRENCY_KEY                =     "Currency"
    let INDEX_KEY                   =     "Index"
    let METAL_KEY                   =     "Metals"
    let EQUITY_KEY                  =     "Equity"
    let SEGMENT_NAME                =     "segment"


