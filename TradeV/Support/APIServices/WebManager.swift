//
//  WebManager.swift
//  TradeV
//
//  Created by Anuj Garg on 23/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Foundation

class WebManager: NSObject {
    
    static var customManager : Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        return Alamofire.SessionManager(configuration: configuration)
        
    }()
    
    class func requestPOSTURL(strURL : String, params : [String : Any]?, success:@escaping (Dictionary<String,AnyObject>) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let status = responseDict[AUTH_OK] as? String
                        {
                            if status == "True"
                            {
                                success(responseDict)
                            }
                            else
                            {
                                if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                                {
                                    
                                    let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                    failure(error)
                                }
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    class func requestForStockList(strURL : String, params : [String : Any]?, success:@escaping ([StockListModel]) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                 MBProgressHUD.hide(for: kAppDelegate.window!, animated: true)
               
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let responseArray = responseDict["stockfutures"] as? [[String : Any]]
                        {
                            var stockModelArray : [StockListModel] = []
                           for responseDic in responseArray
                           {
                            print(responseDic)
                             if let response = responseDic["stockfuture"] as? [String:Any]
                             {
                                
                                stockModelArray.append(StockListModel.fillDataInModel(dict: response))
                             }
                            }
                            
                            success(stockModelArray)
                            
                           
                           
                        }
                        else
                        {
                            if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                            {
                                
                                let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                failure(error)
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    class func requestForIndexList(strURL : String, params : [String : Any]?, success:@escaping ([StockListModel]) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                 MBProgressHUD.hide(for: kAppDelegate.window!, animated: true)
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let responseArray = responseDict["tradeforindexes"] as? [[String : Any]]
                        {
                            var stockModelArray : [StockListModel] = []
                            for responseDic in responseArray
                            {
                                print(responseDic)
                                if let response = responseDic["tradeforindex"] as? [String:Any]
                                {
                                    
                                    stockModelArray.append(StockListModel.fillDataInModel(dict: response))
                                }
                            }
                           
                            success(stockModelArray)
                            
                            
                            
                        }
                        else
                        {
                            if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                            {
                                
                                let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                failure(error)
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    class func requestForMetalList(strURL : String, params : [String : Any]?, success:@escaping ([StockListModel]) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                MBProgressHUD.hide(for: kAppDelegate.window!, animated: true)
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let responseArray = responseDict["tradesformetals"] as? [[String : Any]]
                        {
                            var stockModelArray : [StockListModel] = []
                            for responseDic in responseArray
                            {
                                print(responseDic)
                                if let response = responseDic["tradesformetal"] as? [String:Any]
                                {
                                    
                                    stockModelArray.append(StockListModel.fillDataInModel(dict: response))
                                }
                            }
                            
                            success(stockModelArray)
                            
                            
                            
                        }
                        else
                        {
                            if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                            {
                                
                                let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                failure(error)
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    class func requestForCurrencyList(strURL : String, params : [String : Any]?, success:@escaping ([StockListModel]) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                MBProgressHUD.hide(for: kAppDelegate.window!, animated: true)
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let responseArray = responseDict["tradeforcurrencies"] as? [[String : Any]]
                        {
                            var stockModelArray : [StockListModel] = []
                            for responseDic in responseArray
                            {
                                print(responseDic)
                                if let response = responseDic["tradeforcurrency"] as? [String:Any]
                                {
                                    
                                    stockModelArray.append(StockListModel.fillDataInModel(dict: response))
                                }
                            }
                            
                            success(stockModelArray)
                            
                            
                            
                        }
                        else
                        {
                            if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                            {
                                
                                let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                failure(error)
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    
    class func requestForEquityList(strURL : String, params : [String : Any]?, success:@escaping ([StockListModel]) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                MBProgressHUD.hide(for: kAppDelegate.window!, animated: true)
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let responseArray = responseDict["tradesforequities"] as? [[String : Any]]
                        {
                            var stockModelArray : [StockListModel] = []
                            for responseDic in responseArray
                            {
                                print(responseDic)
                                if let response = responseDic["tradesforequity"] as? [String:Any]
                                {
                                    
                                    stockModelArray.append(StockListModel.fillDataInModel(dict: response))
                                }
                            }
                            
                            success(stockModelArray)
                            
                            
                            
                        }
                        else
                        {
                            if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                            {
                                
                                let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                failure(error)
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    
    class func requestForproductAuthentication(strURL : String, params : [String : Any]?, success:@escaping ([String : Any]) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
            MBProgressHUD.showAdded(to: kAppDelegate.window!, animated: true)
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                //self.printJsonLogs(JSONparams: params, apiUrl: strURL, headers: headers)
                print(parameters)
                print(responseObject)
                MBProgressHUD.hide(for: kAppDelegate.window!, animated: true)
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let responseArray = responseDict["products"] as? [[String : Any]]
                        {
                            
                            for responseDic in responseArray
                            {
                                print(responseDic)
                                if let response = responseDic["product"] as? [String:Any]
                                {
                                    
                                    print(response)
                                    success(response)
                                }
                            }
                            
                           
                            
                            
                            
                        }
                        else
                        {
                            if let errMsg = responseDict["AuthError"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                            {
                                
                                let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                failure(error)
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    
    
    class func requestForLastUpdatedValue(strURL : String, params : [String : Any]?, success:@escaping (String) -> Void, failure:@escaping (NSError) -> Void)
    {
        if let parameters = params{
           
            Alamofire.request(strURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                if responseObject.result.isSuccess
                {
                    if let responseDict = responseObject.result.value as? Dictionary<String,AnyObject>
                    {
                        if let AuthOk = responseDict["AuthOK"] as? String
                        {
                            
                            if AuthOk == "True"
                            {
                                if let lastUpdatedValue = responseDict["LastUpdated"] as? String
                                {
                                     success(lastUpdatedValue)
                                }
                               
                            }
                            else
                            {
                                if let errMsg = responseDict["message"] as? String ,let errorCode = responseDict["errorcode"] as? Int
                                {
                                    
                                    let error = NSError(domain: TRADE_V_SERVER_ERROR, code: errorCode, userInfo: [NSLocalizedDescriptionKey:errMsg])
                                    failure(error)
                                }
                            }
                        }
                    }
                }
                if responseObject.result.isFailure
                {
                    if let error = responseObject.result.error
                    {
                        failure(error as NSError)
                    }
                    else
                    {
                        let error = NSError(domain: kUnknownError, code: 0, userInfo: [NSLocalizedDescriptionKey: "Please try again after some time."])
                        failure(error)
                    }
                }
            }
        }
    }
    class func printJsonLogs(JSONparams:[String : AnyObject]?,apiUrl:String, headers:[String: String]? = nil)-> Void
    {
        var data:NSData?
        
        if let JSONData = JSONparams {
            
            do{
                data = try JSONSerialization.data(withJSONObject: JSONData, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?
            }
            catch
            {
                print(error)
            }
            if let jsondata = data {
                let json = NSString(data: jsondata as Data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    print("hitting api on url: \(apiUrl),  with param: \(json), hearders: \(String(describing: headers)) ")            }
                else {
                    print("hitting api on url: \(apiUrl),  with param: is empty, hearders: \(String(describing: headers)) ")              }
            }
        }
        else {
            print("hitting api on url: \(apiUrl),  with param: is empty, hearders: \(String(describing: headers)) ")          }
    }
    
    
    class func getHeaders() ->   [String : String]
    {
        
        return ["Content-Type" :"application/json"]
    }
}
