//
//  SideMenuModel.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit
//
//struct Segment {
//    var segmentName: String!
//    var segmentImageName: String!
////    var isSegmentSelected: String!
////    var segmentArray : [String]!
//
//    init(segmentName: String? = "", segmentImageName: String? = "") {
//        self.segmentName = segmentName
//        self.segmentImageName = segmentImageName
//    }
//
//}


class SideMenuModel: NSObject
{
    var segmentName = ""
    var segmentImageName = ""
    var isSegmentSelected = false
    var segmentArray : [String] = []
    
    class func fillDataInModel(dict : [String : Any]) -> SideMenuModel
    {
        
        
        let sideMenuModel = SideMenuModel()
        if let name = dict["segmentName"] as? String
        {
            sideMenuModel.segmentName = name
        }
        if let imageName = dict["segmentImageName"] as? String
        {
            sideMenuModel.segmentImageName = imageName
        }
        if let segmentArray = dict["segmentArray"] as? [String]
        {
            sideMenuModel.segmentArray = segmentArray
        }
        sideMenuModel.isSegmentSelected = false
        return sideMenuModel
    }
    
//    func createArr() {
//
//        self.arrDataStore.append(Segment(segmentName: "Main", segmentImageName: "Hello"))
//    }
}
