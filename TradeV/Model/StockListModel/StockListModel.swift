//
//  StockListModel.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
class StockListModel: NSObject
{
   lazy var SNo = ""
   lazy var symbol = ""
   lazy var lotSize = ""
   lazy var signalDate = ""
   lazy var executedSignal = ""
   lazy var lTp = ""
   lazy var stopLoss = ""
   lazy var target = ""
   lazy var trendStrength = ""
   lazy var newSignal = ""
   lazy var newSL = ""
   lazy var newTarget = ""
   lazy var sector = ""
   lazy var timeFrame = ""
   lazy var lastUpdated = ""
   lazy var units = ""
   lazy var quantity = ""
   lazy var newQuantity = ""
   lazy var sectorValue = ""
    
     func checkValueExistOrNot(dict : [String : Any])
     {
        if let symbol = dict["Symbol"] as? String
        {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Stock")
            fetchRequest.predicate = NSPredicate(format: "symbol == %@", symbol)
            var stock : [Stock] = []
            
            do
            {
                stock = try kAppDelegate.persistentContainer.viewContext.fetch(fetchRequest) as! [Stock]
                if stock.count > 0
                {
                    var stockObj = stock.first
                    
                    
                }
                else
                {
                    print("Notfound" )
                }
                
                
            }
            catch
            {
                
            }
        }
        
    }

    
     class func fillDataInModel(dict : [String : Any]) -> StockListModel
     {
        let stockListModel = StockListModel()
        stockListModel.checkValueExistOrNot(dict: dict)
        let context = kAppDelegate.persistentContainer.viewContext
        let stock = Stock(context: context)
        if let newSL = dict["NewSL"] as? String
        {
             stock.newSL = newSL
        }
        if let sector = dict["Sector"] as? String
        {
            stock.sectorValue = sector
        }
        if let newQuantity = dict["NewQty"] as? String
        {
            stock.newQuantity = newQuantity
        }
        
        if let newTarget = dict["NewTarget"] as? String
        {
            stock.newTarget = newTarget
        }
        if let symbol = dict["Symbol"] as? String
        {
            stock.symbol = symbol
        }
        if let executedSignal = dict["ExecutedSig"] as? String
        {
            stock.executedSig = executedSignal
        }
        if let trendStrength = dict["TrendStrength"] as? String
        {
            stock.trendStrength = trendStrength
        }
        if let newSignal = dict["NewSignal"] as? String
        {
            stock.newSignal = newSignal
        }
        if let stopLoss = dict["Stoploss"] as? String
        {
            stock.stopLoss = stopLoss
        }
        if let target = dict["Target"] as? String
        {
            stock.target = target
        }
        if let stopLoss = dict["Stoploss"] as? String
        {
            stock.stopLoss = stopLoss
        }
        if let timeFrame = dict["Timeframe"] as? String
        {
            stock.timeframe = timeFrame
        }
        if let units = dict["Units"] as? String
        {
            stock.units = units
        }
        if let sector = dict["Sector"] as? String
        {
            stock.sector = sector
           
        }
        if let qty = dict["Qty"] as? String
        {
            stock.quantity = qty
        }
        if let ltp = dict["LTP"] as? String
        {
            stock.ltp = ltp
          
        }
        if let lotSize = dict["Lotsize"] as? String
        {
            stock.lotSize = lotSize
           
        }
        if let signalDate = dict["SignalDate"] as? String
        {
            stock.signalDate = signalDate
           
        }
        
        kAppDelegate.saveContext()
      
        return stockListModel
    }
    
    
}
