//
//  Bundle+Extensions.swift
//
//  Created by Anuj on 16/08/17.
//  Copyright © 2017 Anuj. All rights reserved.
//

import Foundation

extension Bundle {
    
    static var versionNumber: String? {
        
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    static var buildNumber: String? {
        return Bundle.main.infoDictionary?["CFBundleVersion"] as? String
    }
}
