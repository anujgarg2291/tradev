//
//  Int+Extension.swift
//
//  Created by Anuj on 22/08/17.
//  Copyright © 2017 Anuj. All rights reserved.
//

import Foundation
import CoreGraphics

extension Int {
    
    var stringValue: String {
        return "\(self)"
    }
}

extension CGFloat {
    var int: Int {
        return Int(self)
    }
}

