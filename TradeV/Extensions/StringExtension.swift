//
//  StringExtension.swift
//  TradeV
//
//  Created by Anuj Garg on 17/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift



extension String {
    
    /// Check if string is valid email format.
    ///
    /// "john@doe.com".isEmail -> true
    /// if john.com.isEmail -> it returns false
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: characters.count)) != nil
    }
}
