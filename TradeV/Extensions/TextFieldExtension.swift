//
//  TextFieldExtension.swift
//  TradeV
//
//  Created by Anuj on 16/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

extension UITextField {
    
    /// Clear text.
    public func clear() {
        text = ""
        attributedText = NSAttributedString(string: "")
    }
    
    /// Set placeholder text color.
    ///
    /// - Parameter color: placeholder text color.
    public func addPlaceHolderTextColor(_ color: UIColor) {
        guard let holder = placeholder, !holder.isEmpty else {
            return
        }
        self.attributedPlaceholder = NSAttributedString(string: holder, attributes: [NSForegroundColorAttributeName: color])
    }
    
    /// Add padding to the left of the textfield rect.
    ///
    /// - Parameter padding: amount of padding to apply to the left of the textfield rect.
    public func addPaddingLeft(_ padding: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: frame.height))
        leftView = paddingView
        leftViewMode = .always
    }
    
    /// Add padding to the left of the textfield rect.
    ///
    /// - Parameters:
    ///   - image: left image
    ///   - padding: amount of padding between icon and the left of textfield
    public func addPaddingLeftIcon(_ image: UIImage, padding: CGFloat) {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        self.leftView = imageView
        self.leftView?.frame.size = CGSize(width: image.size.width + padding, height: image.size.height)
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    
    
    
    
    
    @IBInspectable
    public var placeHolderColor: UIColor? {
        get {
            return self.value(forKeyPath: "_placeholderLabel.textColor") as? UIColor
        } set {
            self.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
        }
        
    }
//
//    @IBInspectable
//    /// Left view tint color.
//    public var leftViewTintColor: UIColor? {
//        get {
//            guard let iconView = leftView as? UIImageView else {
//                return nil
//            }
//            return iconView.tintColor
//        }
//        set {
//            guard let iconView = leftView as? UIImageView else {
//                return
//            }
//            iconView.image = iconView.image?.withRenderingMode(.alwaysTemplate)
//            iconView.tintColor = newValue
//        }
//    }
//    
//    @IBInspectable
//    /// Right view tint color.
//    public var rightViewTintColor: UIColor? {
//        get {
//            guard let iconView = rightView as? UIImageView else {
//                return nil
//            }
//            return iconView.tintColor
//        }
//        set {
//            guard let iconView = rightView as? UIImageView else {
//                return
//            }
//            iconView.image = iconView.image?.withRenderingMode(.alwaysTemplate)
//            iconView.tintColor = newValue
//        }
//    }
    
}
