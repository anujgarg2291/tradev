//
//  UIStoryBoardExtension.swift
//  TradeV
//
//  Created by Anuj on 16/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import Foundation
import Foundation
import UIKit
extension UIStoryboard{
    class func loadViewController<T:UIViewController>(storyBoardName:String,identifierVC:String,type:T.Type,function : String = #function) -> T
    {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        guard let controller = storyboard.instantiateViewController(withIdentifier: identifierVC) as? T else
        {
         fatalError("ViewController with identifier \(identifierVC), not found in  Storyboard.\nFile : \(storyBoardName) \n\nFunction : \(function)")
        }
        return controller
    }
}


enum StoryboardType: String {
    case Main
    case SideMenu
    var fileName : String{
        return rawValue
    }
    
}
