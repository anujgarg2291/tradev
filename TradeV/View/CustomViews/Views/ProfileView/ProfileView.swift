//
//  ProfileView.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class ProfileView: UIView
{
//MARK: Outlets
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var demoInfo: UILabel!
    @IBOutlet weak var userName: UILabel!
    
//MARK: View Instance
    class func instanceFromNib() -> ProfileView
    {
        let profileView = UINib(nibName: "ProfileView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ProfileView
        
        return profileView
        
    }
    
}
