//
//  SegmentView.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class SegmentView: UIView {
    //MARK: Outlets and Variables
 @IBOutlet weak var segmentName: UILabel!
  @IBOutlet weak var segmentButton: UIButton!
//MARK: View Instance
    class func instanceFromNib() -> SegmentView
    {
        let segmentView = UINib(nibName: "SegmentView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SegmentView
        return segmentView
    }
    

}
