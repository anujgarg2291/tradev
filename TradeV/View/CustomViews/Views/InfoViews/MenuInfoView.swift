//
//  MenuInfoView.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class MenuInfoView: UIView {

    @IBOutlet weak var segmentImage: UIImageView!
    @IBOutlet weak var segmentName: UILabel!
    @IBOutlet weak var segmentButton: UIButton!
     @IBOutlet weak var plusImage: UIImageView!
    
    class func instanceFromNib() -> MenuInfoView
    {
        let menuInfoView = UINib(nibName: "MenuInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MenuInfoView
        
        return menuInfoView
        
    }
}
