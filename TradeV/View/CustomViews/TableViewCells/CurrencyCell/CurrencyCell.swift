//
//  CurrencyCell.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell
{
    
    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var executedSignal: UILabel!
    @IBOutlet weak var timeFrame: UILabel!
    @IBOutlet weak var newSignal: UILabel!
    @IBOutlet weak var stopLoss: UILabel!
     @IBOutlet weak var units: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataOnCell(dict : StockListModel)
    {
        self.symbol.text = dict.symbol
        self.executedSignal.text = dict.executedSignal
        self.stopLoss.text = dict.stopLoss
        self.newSignal.text = dict.newSignal
        self.units.text = dict.units
    }
    
    
}
