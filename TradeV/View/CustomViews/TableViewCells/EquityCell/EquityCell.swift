//
//  EquityCell.swift
//  TradeV
//
//  Created by Anuj Garg on 24/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class EquityCell: UITableViewCell
{

    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var executedSignal: UILabel!
    
    @IBOutlet weak var newSignal: UILabel!
   
    @IBOutlet weak var sectorName: UILabel!
    @IBOutlet weak var quantity: UILabel!
    
   
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    func setDataOnCell(dict : StockListModel)
    {
        self.symbol.text = dict.symbol
        self.executedSignal.text = dict.executedSignal
        self.sectorName.text = dict.sector
        self.newSignal.text = dict.newSignal
        self.quantity.text = dict.quantity
      
    }
    
    
}
