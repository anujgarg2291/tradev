//
//  StockCell.swift
//  TradeV
//
//  Created by Anuj Garg on 24/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class StockCell: UITableViewCell
{

    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var executedSignal: UILabel!
    @IBOutlet weak var trendStrength: UILabel!
    @IBOutlet weak var newSignal: UILabel!
    @IBOutlet weak var stopLoss: UILabel!
    @IBOutlet weak var targetValue: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataOnCell(dict : Stock)
    {
        
        self.symbol.text = dict.symbol
        self.executedSignal.text = dict.executedSig
        self.trendStrength.text = dict.trendStrength
        self.newSignal.text = dict.newSignal
        self.targetValue.text = dict.target
        self.stopLoss.text = dict.stopLoss
    }

  
}
