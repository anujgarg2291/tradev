//
//  StockDetailCell.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class StockDetailCell: UITableViewCell
{
    
    @IBOutlet weak var lotSize: UILabel!
    @IBOutlet weak var executedSignal: UILabel!
    @IBOutlet weak var trendStrength: UILabel!
    @IBOutlet weak var newSignal: UILabel!
    @IBOutlet weak var stopLoss: UILabel!
    @IBOutlet weak var signalDate: UILabel!
    @IBOutlet weak var targetValue: UILabel!
    @IBOutlet weak var newStopLoss: UILabel!
    @IBOutlet weak var newTarget: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    func fillDataInCell(dict : StockListModel)
    {
        self.lotSize.text = dict.lotSize
        self.executedSignal.text = dict.executedSignal
        self.trendStrength.text = dict.trendStrength
        self.newSignal.text = dict.newSignal
        self.stopLoss.text = dict.stopLoss
        self.signalDate.text = dict.signalDate
        self.targetValue.text = dict.target
        self.newStopLoss.text = dict.newSL
        self.newTarget.text = dict.newTarget
    }
    
}
