//
//  CurrencyDetailCell.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class CurrencyDetailCell: UITableViewCell
{
    @IBOutlet weak var lotSize: UILabel!
    @IBOutlet weak var executedSignal: UILabel!
    @IBOutlet weak var timeFrame: UILabel!
    @IBOutlet weak var newSignal: UILabel!
    @IBOutlet weak var stopLoss: UILabel!
    @IBOutlet weak var signalDate: UILabel!

    @IBOutlet weak var newStopLoss: UILabel!
    @IBOutlet weak var newTarget: UILabel!
    @IBOutlet weak var lotsLabel: UILabel!
    @IBOutlet weak var stopLossLabel: UILabel!
    @IBOutlet weak var timeFrameLabel: UILabel!
    @IBOutlet weak var newSLLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    func fillDataInCell(dict : StockListModel)
    {
        
        self.executedSignal.text = dict.executedSignal
        self.timeFrame.text = dict.timeFrame
        self.newSignal.text = dict.newSignal
        self.stopLoss.text = dict.stopLoss
        self.signalDate.text = dict.signalDate
       // self.targetValue.text = dict.target
        self.newStopLoss.text = dict.newSL
      //  self.newTarget.text = dict.newTarget
    }
    
    func fillDataForEquityCell(dict : StockListModel)
    {
        self.executedSignal.text = dict.executedSignal
        self.timeFrame.text = dict.sectorValue
        self.newSignal.text = dict.newSignal
        self.stopLoss.text = dict.newQuantity
        self.signalDate.text = dict.signalDate
        // self.targetValue.text = dict.target
        self.newStopLoss.text = dict.newSL
        self.lotSize.text = dict.quantity
        
        
        
        self.newSLLabel.isHidden = true
        self.timeFrameLabel.text = "Sector"
        self.lotsLabel.text = "Qunatity"
        self.stopLossLabel.text =  "New Quantity"
        
    }
    
}
