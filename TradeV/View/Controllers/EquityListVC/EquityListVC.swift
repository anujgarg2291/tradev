//
//  EquityListVC.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class EquityListVC: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    
    @IBOutlet weak var equityListTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
     @IBOutlet weak var lastUpDatedValue:UILabel!
    var equityListModelArray : [StockListModel] = []
    //MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.async
        {
                self.callAPIForLastUpdatedValue()
        }
        self.callAPIForEquityList()
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    
    func callAPIForLastUpdatedValue()
    {
        let lastUpDatedRequest = [KEY_DEFAULT : SEGMENT_NAME, SEGMENT_NAME : EQUITY_KEY] as [String : Any]
        WebManager.requestForLastUpdatedValue(strURL: BASE_URL+LAST_UPDATED_VALUE, params: lastUpDatedRequest, success: { (response) in
            if response.count > 0
            {
                self.lastUpDatedValue.text = "Last Upadte : " + response
                
            }
        })
        { (error) in
            
        }
    }
    
    func callAPIForEquityList()
    {
        let indexListRequest = [KEY_DEFAULT : SEGEMENT_KEY ] as [String : Any]
        WebManager.requestForEquityList(strURL: BASE_URL+EQUITY_LIST_URL, params: indexListRequest, success: { (response) in
            if response.count > 0
            {
                self.equityListModelArray = response
                self.equityListTableView.reloadData()
            }
        })
        { (error) in
            
        }
    }
    
    //MARK: IBAction on Button
    @IBAction func menuButtonClicked(_ sender: Any)
    {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    
    //MARK: Configure TableView
    func configureTableView()
    {
        // stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        equityListTableView.register(UINib(nibName: "EquityCell", bundle: nil), forCellReuseIdentifier: "EquityCell")
        self.equityListTableView.rowHeight = UITableViewAutomaticDimension
        self.equityListTableView.estimatedRowHeight = 250
        self.equityListTableView.separatorStyle = .none
    }
    
    //MARK: TableView DataSource and Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.equityListModelArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.equityListTableView.dequeueReusableCell(withIdentifier: "EquityCell", for: indexPath)  as! EquityCell
        cell.setDataOnCell(dict: self.equityListModelArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC:CURRENCY_DETAIL_VC_IDENTIFIER, type:  CurrencyDetailController.self)
        controller.stockListData = self.equityListModelArray[indexPath.row]
        controller.fileName = FILE_NAME.EQUITY.rawValue
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
