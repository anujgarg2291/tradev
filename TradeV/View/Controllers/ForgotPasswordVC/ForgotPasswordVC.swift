//
//  ForgotPasswordVC.swift
//  TradeV
//
//  Created by Anuj on 16/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController
{
//MARK: Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var infoLabel: UILabel!
    
    
//MARK: Controller life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.emailTextField.addPaddingLeft(LEFT_PADDING_VALUE)
        self.infoLabel.text = FORGOT_PASSWORD_INFO
        self.emailTextField.addPlaceHolderTextColor(PLACEHOLDER_TEXT_COLOR)
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.addObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        self.removeObserver()
    }
    
//MARK: IBAction on Button
    @IBAction func submitButtonClicked(_ sender: UIButton)
    {
        
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
//MARK: Add and remove Obeserver
    func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        TAUtility.showKeyBoard(notification: notification, scrollView: self.scrollView)
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        TAUtility.hideKeyBoard(scrollView: self.scrollView)
    }
}
