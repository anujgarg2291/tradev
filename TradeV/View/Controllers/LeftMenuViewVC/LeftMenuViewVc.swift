//
//  LeftMenuViewVc.swift
//  TradeV
//
//  Created by Anuj Garg on 25/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class LeftMenuViewVc: UIViewController , UITableViewDataSource , UITableViewDelegate
{
//MARK: Outlets and Variables
    @IBOutlet weak var leftMenuTableView: UITableView!
    var sideMenuArray :[SideMenuModel] = []
    
//MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        let leftMenuModel = LeftMenuViewModel()
        leftMenuModel.fillDataInModel { (response) in
            self.sideMenuArray = response
            self.leftMenuTableView.reloadData()
        }
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: Configure TableView
    func configureTableView()
    {
        // stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        leftMenuTableView.register(UINib(nibName: "SegmentCell", bundle: nil), forCellReuseIdentifier: "SegmentCell")
        self.leftMenuTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.leftMenuTableView.estimatedSectionHeaderHeight = 200
        self.leftMenuTableView.rowHeight = UITableViewAutomaticDimension
        self.leftMenuTableView.estimatedRowHeight = 51
        self.leftMenuTableView.separatorStyle = .none
    }
    
    
    //MARK: Tableview DataSource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.sideMenuArray[section].isSegmentSelected ? self.sideMenuArray[section].segmentArray.count : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.sideMenuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SegmentCell", for: indexPath) as! SegmentCell
        let segmentData = self.sideMenuArray[indexPath.section].segmentArray
        cell.segmentName.text = segmentData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let sideMenuInfo = self.sideMenuArray[section]
        if section == 0
        {
            let  profileView = ProfileView.instanceFromNib()
            return profileView
        }
        if section > 0 && section < 6
        {
            let segmentView = SegmentView.instanceFromNib()
            segmentView.segmentButton.tag = section
            segmentView.segmentButton.addTarget(self, action: #selector(segmentButtonTapped), for: .touchUpInside)
            segmentView.segmentName.text = sideMenuInfo.segmentName
            return segmentView
            
        }
        else
        {
            let menuInfo = MenuInfoView.instanceFromNib()
            menuInfo.segmentName.text = sideMenuInfo.segmentName
            menuInfo.segmentImage.image = UIImage(named: sideMenuInfo.segmentImageName)
            menuInfo.plusImage.isHidden = !(sideMenuInfo.segmentArray.count > 0)
            menuInfo.segmentButton.tag = section
            menuInfo.segmentButton.addTarget(self, action: #selector(segmentButtonTapped), for: .touchUpInside)
            return menuInfo
        }
    }
    
    
    func segmentButtonTapped(sender : UIButton)
    {
        var vc : UIViewController!
        if sender.tag == 1
        {
             vc = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: HOME_VC_IDENTIFIER, type: HomeViewController.self)
            
        }
        
        if sender.tag == 2
        {
            vc = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: INDEX_LIST_VC_IDENTIFIER, type: IndexListVC.self)
           
        }
        if sender.tag == 3
        {
            vc = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: CURRENCY_LIST_VC_IDENTIFIER, type: CurrencyListVC.self)
        }
        if sender.tag == 4
        {
            vc = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: METAL_LIST_VC_IDENTIFIER, type: MetalListVC.self)
        }
        if sender.tag == 5
        {
             vc = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: EQUITY_LIST_VC_IDENTIFIER, type: EquityListVC.self)
        }
        
        if sender.tag == 6 || sender.tag == 8
        {
            self.sideMenuArray[sender.tag].isSegmentSelected = !self.sideMenuArray[sender.tag].isSegmentSelected
            self.leftMenuTableView.reloadData()
            return
        }
        if let vc = vc
        {
            let homeNavigationViewController: UINavigationController = UINavigationController(rootViewController: vc)
            homeNavigationViewController.isNavigationBarHidden = true
            self.sideMenuViewController.setContentViewController(homeNavigationViewController, animated: true)
            self.sideMenuViewController.hideViewController()
        }
        
    }
    
    
}


