	//
//  AccountInfoVc.swift
//  TradeV
//
//  Created by Anuj Garg on 24/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class AccountInfoVc: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func paidLoginButtonClicked(_ sender: Any)
    {
      ///  let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: HOME_VC_IDENTIFIER, type: HomeViewController.self)
     ///   self.navigationController?.pushViewController(controller, animated: true)
        
        kAppDelegate.goToHomeViewVC()
    }
    
    
    @IBAction func purchaseSubscriptionButtonClicked(_ sender: Any)
    {
        
        guard let url = URL(string: "http://www.s2analytics.com/website/index.php") else {
            return //be safe
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        return
        let request = [USER_ID : "102" , KEY_DEFAULT : LOGIN_KEY , INSTALLATION_ID : "1" , MAIN_PRODUCT_ID : "TV"]
        WebManager.requestForproductAuthentication(strURL: BASE_URL + PRODUCT_AUTHENTICATION_URL , params: request, success: { (response) in
            print(response)
            var activatedOnOtherDevice = ""
            var notActivated = ""
            if let activatedValue = response["ActivatedOtherDevice"] as? String
            {
                activatedOnOtherDevice = activatedValue
                
            }
            if let notActivatedValue = response["NotActivated"] as? String
            {
                notActivated = notActivatedValue
            }
            
            if activatedOnOtherDevice == "True" && notActivated == "True"
            {
                
                TAAlertController.alert(APP_NAME, message: PRODUCT_AUTH_MESSAGE, acceptMessage: "OK", acceptBlock: {
                    
                })
            }
            else if notActivated == "True"
            {
               
                TAAlertController.alert(APP_NAME, message: PRODUCT_AUTH_MESSAGE, acceptMessage: "OK", acceptBlock: {
                    
                })
            }
            else if activatedOnOtherDevice == "True"
            {
                TAAlertController.alert(APP_NAME, message: PRODUCT_ACTIVATED_ON_OTHER_DEVICE_MESSAGE, acceptMessage: "OK", acceptBlock: {
                    
                })
            }
            
            
            
        }) { (error) in
            print(error)
        }
        
        
    }
    @IBAction func tryDemoButtonClicked(_ sender: Any)
    {
        kAppDelegate.goToHomeViewVC()
        
       
    }

}
