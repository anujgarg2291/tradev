//
//  CurrencyListVC.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class CurrencyListVC: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    
    @IBOutlet weak var currencyListTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var lastUpDatedValue:UILabel!
    var currencyListModelArray : [StockListModel] = []
    //MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        self.navigationController?.isNavigationBarHidden = true
        
        self.callAPIForCurrencyList()
        DispatchQueue.main.async
            {
                self.callAPIForLastUpdatedValue()
        }
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func callAPIForLastUpdatedValue()
    {
        let lastUpDatedRequest = [KEY_DEFAULT : SEGEMENT_KEY, SEGMENT_NAME : CURRENCY_KEY] as [String : Any]
        WebManager.requestForLastUpdatedValue(strURL: BASE_URL+LAST_UPDATED_VALUE, params: lastUpDatedRequest, success: { (response) in
            if response.count > 0
            {
                self.lastUpDatedValue.text = "Last Upadte : " + response
                
            }
        })
        { (error) in
            
        }
    }
    
    func callAPIForCurrencyList()
    {
        let indexListRequest = [KEY_DEFAULT : SEGEMENT_KEY ] as [String : Any]
        WebManager.requestForCurrencyList(strURL: BASE_URL+CURRENCY_LIST_URL, params: indexListRequest, success: { (response) in
            if response.count > 0
            {
                self.currencyListModelArray = response
                self.currencyListTableView.reloadData()
            }
        })
        { (error) in
            
        }
    }
    
    //MARK: IBAction on Button
    @IBAction func menuButtonClicked(_ sender: Any)
    {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    
    //MARK: Configure TableView
    func configureTableView()
    {
        // stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        currencyListTableView.register(UINib(nibName: "CurrencyCell", bundle: nil), forCellReuseIdentifier: "CurrencyCell")
        self.currencyListTableView.rowHeight = UITableViewAutomaticDimension
        self.currencyListTableView.estimatedRowHeight = 250
        self.currencyListTableView.separatorStyle = .none
    }
    
    //MARK: TableView DataSource and Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currencyListModelArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.currencyListTableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath)  as! CurrencyCell
        cell.setDataOnCell(dict: self.currencyListModelArray[indexPath.row])
        
        return cell
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC:CURRENCY_DETAIL_VC_IDENTIFIER, type:  CurrencyDetailController.self)
        controller.stockListData = self.currencyListModelArray[indexPath.row]
        controller.fileName = FILE_NAME.CURRENCY.rawValue
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
