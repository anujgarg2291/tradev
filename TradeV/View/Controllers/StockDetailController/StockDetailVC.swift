//
//  StockDetailVC.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class StockDetailVC: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    @IBOutlet weak var symbol: UILabel!
    @IBOutlet weak var ltpValue: UILabel!
     @IBOutlet weak var stockDetailTableView: UITableView!
    
    var stockListData : StockListModel?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        self.symbol.text = stockListData?.symbol
        if let ltpValue = stockListData?.lTp
        {
            self.ltpValue.text = "LTP : " + ltpValue
        }
        // Do any additional setup after loading the view.
    }

    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: IBAction on Button
    @IBAction func backdButtonClicked(_ sender: Any)
    {
       self.navigationController?.popViewController(animated: true)
    }
 
    //MARK: Configure TableView
    func configureTableView()
    {
        // stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        stockDetailTableView.register(UINib(nibName: "StockDetailCell", bundle: nil), forCellReuseIdentifier: "StockDetailCell")
       /*& self.stockDetailTableView.rowHeight = UITableViewAutomaticDimension
        self.stockDetailTableView.estimatedRowHeight = 600 */
        self.stockDetailTableView.separatorStyle = .none
    }
    
    //MARK: TableView DataSource and Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if stockListData != nil{
            return 1
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.stockDetailTableView.dequeueReusableCell(withIdentifier: "StockDetailCell", for: indexPath)  as! StockDetailCell
        cell.fillDataInCell(dict: stockListData!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 600
    }
}
