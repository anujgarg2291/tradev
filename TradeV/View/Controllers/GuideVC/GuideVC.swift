//
//  GuideVC.swift
//  TradeV
//
//  Created by Anuj Garg on 27/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class GuideVC: UIViewController,UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout
{

    @IBOutlet var guideCollectionView: UICollectionView!
   @IBOutlet var pageControl: UIPageControl!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        guideCollectionView.register(UINib(nibName: "GuideCell", bundle: nil), forCellWithReuseIdentifier: "GuideCell")
    }

    @IBAction func loginButtonClicked(_ sender: Any)
    {
            let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: LOGIN_VC_IDENTIFIER, type: LoginVC.self)
            self.navigationController?.pushViewController(controller, animated: true)
    }
    
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GuideCell", for: indexPath) as! GuideCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.guideCollectionView.frame.size.width, height: self.guideCollectionView.frame.size.height)
        
    }
    
    //MARK: ScrollView Delegates
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        let pageWidth : CGFloat = self.guideCollectionView.frame.size.width ;
        self.pageControl.currentPage = (self.guideCollectionView.indexPathsForVisibleItems.first?.row)!
        let FractionalPage : CGFloat = scrollView.contentOffset.x / pageWidth
        let y = Int(round(FractionalPage))
        self.pageControl.currentPage = y
    }
}
