//
//  LoginVC.swift
//  TradeV
//
//  Created by Anuj on 16/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class LoginVC: UIViewController , UITextFieldDelegate
{
//MARK: Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
//MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTextField()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.addObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        self.removeObserver()
    }
    
    //MARK: Configure text Field
    func configureTextField()
    {
        self.emailTextField.addPaddingLeft(LEFT_PADDING_VALUE)
        self.passwordTextField.addPaddingLeft(LEFT_PADDING_VALUE)
        self.emailTextField.addPlaceHolderTextColor(PLACEHOLDER_TEXT_COLOR)
        self.passwordTextField.addPlaceHolderTextColor(PLACEHOLDER_TEXT_COLOR)
    }
    
//MARK: IBAction on Button
    @IBAction func forgotPasswordButtonClicked(_ sender: Any)
    {
        let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: FORGOT_PASSWORD_VC_IDENTIFIER, type: ForgotPasswordVC.self)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func loginButtonClicked(_ sender: Any)
    {
        LoginViewModel.isValidAllFields(email: self.emailTextField.text ?? "", password: self.passwordTextField.text ?? "")
        { (response) in
            TAUtility.saveDataInUserDefault(dict: response)
            let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: ACCOUNT_INFO_VC_IDENTIFIER, type: AccountInfoVc.self)
           
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
    
//MARK: Add and remove Obeserver
    func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        TAUtility.showKeyBoard(notification: notification, scrollView: self.scrollView)
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
            TAUtility.hideKeyBoard(scrollView: self.scrollView)
    }
    
    
}
 
