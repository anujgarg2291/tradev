//
//  MetalListVC.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class MetalListVC: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    
    @IBOutlet weak var metalListTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var lastUpDatedValue:UILabel!
    var metalModelArray : [StockListModel] = []
    //MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        self.navigationController?.isNavigationBarHidden = true
        self.callAPIForMetalList()
        DispatchQueue.main.async
            {
                self.callAPIForLastUpdatedValue()
        }
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    func callAPIForLastUpdatedValue()
    {
        let lastUpDatedRequest = [KEY_DEFAULT : SEGEMENT_KEY, SEGMENT_NAME : METAL_KEY] as [String : Any]
        WebManager.requestForLastUpdatedValue(strURL: BASE_URL+LAST_UPDATED_VALUE, params: lastUpDatedRequest, success: { (response) in
            if response.count > 0
            {
                self.lastUpDatedValue.text = "Last Upadte : " + response
                
            }
        })
        { (error) in
            
        }
    }
    
    func callAPIForMetalList()
    {
        let stockListRequest = [KEY_DEFAULT : SEGEMENT_KEY ] as [String : Any]
        WebManager.requestForMetalList(strURL: BASE_URL+METAL_LIST_URL, params: stockListRequest, success: { (response) in
            if response.count > 0
            {
                self.metalModelArray = response
                self.metalListTableView.reloadData()
            }
        })
        { (error) in
            
        }
    }
    
    //MARK: IBAction on Button
    @IBAction func menuButtonClicked(_ sender: Any)
    {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    
    //MARK: Configure TableView
    func configureTableView()
    {
        // stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        metalListTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        self.metalListTableView.rowHeight = UITableViewAutomaticDimension
        self.metalListTableView.estimatedRowHeight = 250
        self.metalListTableView.separatorStyle = .none
    }
    
    //MARK: tableview Delegate and dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.metalModelArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.metalListTableView.dequeueReusableCell(withIdentifier: "StockCell", for: indexPath)  as! StockCell
       // cell.setDataOnCell(dict: self.metalModelArray[indexPath.row])
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: STOCK_DETAIL_VC_IDENTIFIER, type: StockDetailVC.self)
        controller.stockListData = self.metalModelArray[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
