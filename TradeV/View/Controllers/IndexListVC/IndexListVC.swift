//
//  IndexListVC.swift
//  TradeV
//
//  Created by Anuj Garg on 26/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class IndexListVC: UIViewController, UITableViewDataSource , UITableViewDelegate
{

    @IBOutlet weak var indexListTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var lastUpDatedValue:UILabel!
    var indexListModelArray : [StockListModel] = []
    //MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.async
            {
                self.callAPIForLastUpdatedValue()
        }
        self.callAPIForIndexList()
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    
    func callAPIForLastUpdatedValue()
    {
        let lastUpDatedRequest = [KEY_DEFAULT : SEGEMENT_KEY, SEGMENT_NAME : INDEX_KEY] as [String : Any]
        WebManager.requestForLastUpdatedValue(strURL: BASE_URL+LAST_UPDATED_VALUE, params: lastUpDatedRequest, success: { (response) in
            if response.count > 0
            {
                self.lastUpDatedValue.text = "Last Upadte : " + response
                
            }
        })
        { (error) in
            
        }
    }

    func callAPIForIndexList()
    {
        let indexListRequest = [KEY_DEFAULT : SEGEMENT_KEY ] as [String : Any]
        WebManager.requestForIndexList(strURL: BASE_URL+INDEX_LIST_URL, params: indexListRequest, success: { (response) in
            if response.count > 0
            {
                self.indexListModelArray = response
                self.indexListTableView.reloadData()
            }
        })
        { (error) in
            
        }
    }
    
    //MARK: IBAction on Button
    @IBAction func menuButtonClicked(_ sender: Any)
    {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    
    //MARK: Configure TableView
    func configureTableView()
    {
        indexListTableView.register(UINib(nibName: "IndexCell", bundle: nil), forCellReuseIdentifier: "IndexCell")
        self.indexListTableView.rowHeight = UITableViewAutomaticDimension
        self.indexListTableView.estimatedRowHeight = 250
        self.indexListTableView.separatorStyle = .none
    }
    
    //MARK: TableView DataSource and Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.indexListModelArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.indexListTableView.dequeueReusableCell(withIdentifier: "IndexCell", for: indexPath)  as! IndexCell
        cell.setDataOnCell(dict: self.indexListModelArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC:CURRENCY_DETAIL_VC_IDENTIFIER, type:  CurrencyDetailController.self)
        controller.stockListData = self.indexListModelArray[indexPath.row]
        controller.fileName = FILE_NAME.INDEX.rawValue
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
