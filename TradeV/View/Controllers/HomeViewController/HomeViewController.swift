//
//  HomeViewController.swift
//  TradeV
//
//  Created by Anuj Garg on 24/10/17.
//  Copyright © 2017 TechAhead Pvt. Ltd. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource , UITableViewDelegate {

  @IBOutlet weak var stockTableView: UITableView!
  @IBOutlet weak var lastUpDatedValue:UILabel!
  @IBOutlet weak var menuButton: UIButton!
    var stockList : [Stock] = []
    var stockModelArray : [StockListModel] = []
    //MARK: Controller Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureTableView()
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.async
        {
            self.callAPIForLastUpdatedValue()
        }
        self.callAPIForStockList()
        
    }

    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func callAPIForLastUpdatedValue()
    {
        let lastUpDatedRequest = [KEY_DEFAULT : SEGMENT_NAME, SEGMENT_NAME : STOCK_KEY] as [String : Any]
        WebManager.requestForLastUpdatedValue(strURL: BASE_URL+LAST_UPDATED_VALUE, params: lastUpDatedRequest, success: { (response) in
            if response.count > 0
            {
                self.lastUpDatedValue.text = "Last Upadte : " + response
                
            }
        })
        { (error) in
            
        }
    }
    
    
    func getData()
    {
        let context = kAppDelegate.persistentContainer.viewContext
        do
        {
         stockList =   try context.fetch(Stock.fetchRequest())
        }
        catch
        {
            TAAlertController.alert(APP_NAME, message: "Fetching Failed")
        }
        self.stockTableView.reloadData()
    }
    
    func callAPIForStockList()
    {
        let stockListRequest = [KEY_DEFAULT : SEGEMENT_KEY ] as [String : Any]
        WebManager.requestForStockList(strURL: BASE_URL+STOCK_LIST_URL, params: stockListRequest, success: { (response) in
            if response.count > 0
            {
                self.getData()
              //  self.stockModelArray = response
               // self.stockTableView.reloadData()
            }
        })
        { (error) in
            
        }
    }
    
    //MARK: IBAction on Button
    @IBAction func menuButtonClicked(_ sender: Any)
    {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    
    //MARK: Configure TableView
    func configureTableView()
    {
       // stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        stockTableView.register(UINib(nibName: "StockCell", bundle: nil), forCellReuseIdentifier: "StockCell")
        self.stockTableView.rowHeight = UITableViewAutomaticDimension
        self.stockTableView.estimatedRowHeight = 250
        self.stockTableView.separatorStyle = .none
    }
   
    //MARK: tableview Delegate and dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.stockList.count
    }
  
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.stockTableView.dequeueReusableCell(withIdentifier: "StockCell", for: indexPath)  as! StockCell
        cell.setDataOnCell(dict: self.stockList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let controller = UIStoryboard.loadViewController(storyBoardName: StoryboardType.Main.fileName, identifierVC: STOCK_DETAIL_VC_IDENTIFIER, type: StockDetailVC.self)
        controller.stockListData = self.stockModelArray[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
